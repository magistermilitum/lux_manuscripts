
// Listar en el menú desplegable los títulos de los manuscritos en h4

$(document).ready(function() {
  // Obtener todos los elementos h5 de la página
  var h4Elements = $('h4');
  // Recorrer cada elemento h5
  h4Elements.each(function() {
    // Obtener el texto del elemento h5
    var h4Text = $(this).text();
    // Crear un nuevo elemento a con el texto del elemento h5
    var aElement = $('<a class="dropdown-item">' + h4Text + '</a>');
    // Añadir el elemento a al menú desplegable
    $('.dropdown-menu').append(aElement);
  });
 });
 
 
 
 
 // Barra de búsqueda: resaltar las palabras buscadas en los resultados de búsqueda y eliminar los div que no coinciden con el término de búsqueda
 
 // Obtener el formulario de búsqueda y el campo de entrada
 const searchForm = document.querySelector('.d-flex');
 const searchInput = document.querySelector('.form-control');
 
 // Función para resaltar las palabras buscadas en un nodo
 function highlightSearchTerm(node, searchTerm) {
  // Comprobar si el nodo es un nodo de texto
  if (node.nodeType === Node.TEXT_NODE) {
    // Crear una expresión regular para buscar el término de búsqueda en el contenido del nodo
    const regex = new RegExp(searchTerm, 'gi');
 
    // Reemplazar todas las ocurrencias del término de búsqueda con un elemento <span> para resaltarlo
    if (regex.test(node.textContent)) {
      const span = document.createElement('span');
      span.innerHTML = node.textContent.replace(regex, '<span class="search-highlight">$&</span>');
      node.parentNode.replaceChild(span, node);
    }
  } else {
    // Recorrer todos los nodos hijos del nodo
    node.childNodes.forEach(function(childNode) {
      highlightSearchTerm(childNode, searchTerm);
    });
  }
 }
 
 // Agregar un controlador de eventos al formulario para detectar cuando se envía
 searchForm.addEventListener('submit', function(event) {
  // Evitar que el formulario se envíe y recargue la página
  event.preventDefault();
 
  // Obtener el valor del campo de búsqueda
  const searchTerm = searchInput.value.toLowerCase();
 
  // Buscar el término de búsqueda en los elementos relevantes de la página
  const divs = document.querySelectorAll('.row-book');
  divs.forEach(function(div) {
    // Obtener todos los elementos relevantes dentro del div
    const elements = div.querySelectorAll('p, h1, h2, h3, h4, h5, h6, a');
 
    // Comprobar si alguno de los elementos contiene el término de búsqueda
    let found = false;
    elements.forEach(function(element) {
      // Eliminar cualquier resaltado anterior
      const highlights = element.querySelectorAll('.search-highlight');
      highlights.forEach(function(highlight) {
        highlight.parentNode.replaceChild(document.createTextNode(highlight.textContent), highlight);
      });
 
      if (element.textContent.toLowerCase().includes(searchTerm)) {
        found = true;
 
        if (searchTerm !== '') {
          // Resaltar las palabras buscadas en el elemento
          highlightSearchTerm(element, searchTerm);
        }
      }
    });
 
    if (found || searchTerm === '') {
      // El div contiene el término de búsqueda o el término de búsqueda está vacío
      // Mostrar el div
      div.style.display = '';
    } else {
      // El div no contiene el término de búsqueda y el término de búsqueda no está vacío
      // Ocultar el div
      div.style.display = 'none';
    }
  });
 });
 
 